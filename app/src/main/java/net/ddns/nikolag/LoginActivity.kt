package net.ddns.nikolag

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.JsonReader
import android.util.Log
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputEditText
import org.json.JSONArray
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset




class LoginActivity : AppCompatActivity() {

    private lateinit var button: Button
    private lateinit var username: TextInputEditText
    private lateinit var password: TextInputEditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // onclick for swap to register activity
        findViewById<Button>(R.id.to_registation).setOnClickListener {

            val intent = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(intent)

            finish()
        }

        button = findViewById(R.id.button)
        username = findViewById(R.id.textUsername)
        password = findViewById(R.id.textPassword)




        button.setOnClickListener(){


            val con = KolibriApiMaker()


            // register -> val params = con.callRegister("1", "2", "3", "4")
            val params = con.callLogin(username.text.toString(), password.text.toString())


            val queue = Volley.newRequestQueue(this)

            val url = "https://nikolag.ddns.net/ian/index.php"

            val jsonOb = JSONObject()
            jsonOb.put("type", "register")

            Log.e("BEFOREW SEND", params.toString())

            val stringRequest = object: StringRequest(Request.Method.POST, url,
                object: Response.Listener<String>{
                    override fun onResponse(response: String?) {
                        Log.d("Response",response)
                        if(response?.trim() == "1"){
                            Toast.makeText(this@LoginActivity, "Prijava ok", Toast.LENGTH_LONG).show()

                            AccountSingleton.instance.email = username.text.toString()
                            AccountSingleton.instance.rentBooks = ArrayList<BookClass>()
                            // start activity
                            val intent = Intent(this@LoginActivity, LandingActivity::class.java)
                            startActivity(intent)

                            finish()
                        }else{
                            Toast.makeText(this@LoginActivity, response, Toast.LENGTH_LONG).show()
                        }
                    }
                },
                object : Response.ErrorListener{
                    override fun onErrorResponse(error: VolleyError?) {
                        Log.d("Error",error.toString())
                        Toast.makeText(this@LoginActivity, error?.message, Toast.LENGTH_SHORT).show()
                    }
                }){
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/x-www-form-urlencoded"
                    return headers
                }

                override fun getParams(): MutableMap<String, String> {
                    return params
                }
            }

            // Access the RequestQueue through your singleton class.
            queue.add(stringRequest)


            return@setOnClickListener




       }




    }

}
