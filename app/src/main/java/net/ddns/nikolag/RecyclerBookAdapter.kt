package net.ddns.nikolag

import android.content.Intent
import android.graphics.Color
import android.opengl.Visibility
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.MotionEventCompat
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.book_recyclerview_item_row.*
import kotlinx.android.synthetic.main.book_recyclerview_item_row.view.*


class RecyclerBookAdapter(private val bookList: ArrayList<BookClass>) : RecyclerView.Adapter<RecyclerBookAdapter.TextHolder>(){

    private var bookListCheck = ArrayList<Boolean>()
    private var multiselectEnable = false


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerBookAdapter.TextHolder {
        for (x in bookList.indices){
            bookListCheck.add(false)
        }

        val inflatedView = parent.inflate(R.layout.book_recyclerview_item_row, false)
        return TextHolder(inflatedView, this)
    }

    override fun getItemCount(): Int {
        return bookList.size
    }

    class TextHolder(v: View, r: RecyclerBookAdapter) : RecyclerView.ViewHolder(v), View.OnClickListener, View.OnLongClickListener, View.OnKeyListener {
        //2
        private var view: View = v
        private var book: BookClass? = null
        private val recParent: RecyclerBookAdapter = r
        private var pos: Int = 0
        private var checkboxEnable = false

        //3
        init {
            v.setOnClickListener(this)
            v.setOnLongClickListener(this)
            v.setOnKeyListener(this)
            v.checkBox.setOnClickListener(this)
           // v.setOnTouchListener(this)
        }
        //4
        /*
        override fun onClick(v: View) {
            Log.d("RecyclerView", "CLICK!")
            Toast.makeText(v.context, this.book?.name, Toast.LENGTH_SHORT).show()
            /*
            val context = itemView.context
            val showPhotoIntent = Intent(context, MainActivity::class.java)
            showPhotoIntent.putExtra(TEXT_KEY, text)
            context.startActivity(showPhotoIntent)*/

        }*/

        companion object {
            //5
            private val TEXT_KEY = "TEXT"
        }

        fun toggleCheckbox(){
            checkboxEnable = !checkboxEnable
        }

        fun showCheckbox(){
            view.checkBox.visibility=View.VISIBLE
        }

        fun hideCheckbox(){
            view.checkBox.visibility=View.GONE
        }

        fun checkboxTrue(){
            view.checkBox.isChecked = true
        }
        fun checkboxFalse(){

        }

        fun toggleCheckboxSelection(){
            view.checkBox.isChecked = !view.checkBox.isChecked
            recParent.bookListCheck[pos] = checkboxStatus()
        }

        fun toggleBg(t: Boolean){
            /*
            if(t)
                view.setBackgroundColor(Color.RED)

            else{
                view.setBackgroundResource(R.drawable.book_rec_row_border)
            }*/

            val intent = Intent(this.view.context, BookViewActivity::class.java).apply {
                putExtra("BOOK_NAME", book?.name)
                putExtra("BOOK_ISBN", book?.isbn)
                putExtra("BOOK_IMG", book?.image)
                putExtra("BOOK_AUTHOR", book?.author)
                putExtra("BOOK_DATE", book?.date)
                putExtra("BOOK_PUBLISHER", book?.publisher)
                putExtra("BOOK_LANG", book?.language)
                putExtra("BOOK_PAGES", book?.pages)



            }
            this.view.context.startActivity(intent)
         }

        fun toggleCheckbox(t: Boolean){
            // toggle it
            recParent.multiselectEnable = true

        }

        fun checkboxStatus(): Boolean{
            return view.checkBox.isChecked
        }


        fun bindText(book: BookClass, position: Int) {
            this.book = book
            this.pos = position
            view.textView?.setText(book.name)
            view.textView2?.setText(book.isbn)

            Log.d("TT", "Text binded")
            if(recParent.multiselectEnable == true){
                showCheckbox()
                view.checkBox.isChecked=recParent.bookListCheck[pos]
            }
            else hideCheckbox()

            //view.findViewById(R.id.textView)

            /*Picasso.with(view.context).load(photo.url).into(view.itemImage)
            view.itemDate.text = photo.humanDate
            view.itemDescription.text = photo.explanation*/
        }

        override fun onClick(v: View?) {

            if(recParent.multiselectEnable){
                if(v !is CheckBox){
                    toggleCheckboxSelection()
                }

                recParent.bookListCheck[pos] =  checkboxStatus()


            }

                else{
                    toggleBg(recParent.notifyChange(this.pos))
                }




           /* when(action){
                MotionEvent.ACTION_DOWN -> {

                }
                MotionEvent.ACTION_UP -> {
                    toggleBg(recParent.notifyChange(this.pos))
                }
                else -> {

                    Log.d("action", action.toString())
                }
            }
            //Log.d("ACTION", action.toString())*/

        }

        override fun onLongClick(v: View?): Boolean {
            toggleCheckbox(true);
            toggleCheckboxSelection()
            recParent.notifyDataSetChanged()
            return true
        }

        override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
            Log.e("USPE", "DASY");
            if(this.checkboxEnable == true){

                return false
            }

                return false

        }
    }

    override fun onBindViewHolder(holder: TextHolder, position: Int) {
        val book = bookList[position]
        holder.bindText(book, position)

        Log.e("Wrong", "click")
    }

    fun notifyChange(pos: Int): Boolean{
        this.bookListCheck[pos] = !this.bookListCheck[pos]
        return this.bookListCheck[pos]
    }

    fun toggleMultiselect(){
        multiselectEnable = !multiselectEnable
    }

    fun handleBack(): Boolean{

        if(multiselectEnable == true){

            for(x in 0..bookListCheck.size-1){
                bookListCheck[x] = false
            }

            multiselectEnable = false
            notifyDataSetChanged()
            return true
        }

        return false
    }

}
