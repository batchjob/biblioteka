package net.ddns.nikolag

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray

class MainActivity : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: RecyclerBookAdapter
    private lateinit var texts: ArrayList<String>
    private lateinit var bookList: ArrayList<BookClass>

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.booklist_toolbar, menu)

        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        bookList = ArrayList<BookClass>()


        texts = ArrayList<String>()
        linearLayoutManager = LinearLayoutManager(this)
       // recyclerView.layoutManager = linearLayoutManager
        adapter = RecyclerBookAdapter(bookList)
       // recyclerView.adapter = adapter




        Log.d("tt", "Created")
    }

    override fun onStart() {
        super.onStart()

        if (texts.size == 0) {
            texts.add("tt1")
            texts.add("rr2")
        }

    }

    override fun onBackPressed() {
        var status = adapter.handleBack()
        if(status == false) super.onBackPressed()
    }
}
