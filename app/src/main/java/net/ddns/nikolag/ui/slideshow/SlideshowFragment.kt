package net.ddns.nikolag.ui.slideshow

import android.accounts.Account
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.ddns.nikolag.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class SlideshowFragment : Fragment() {

    private lateinit var slideshowViewModel: SlideshowViewModel

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: RecyclerBookAdapter
    private lateinit var texts: ArrayList<String>
    private lateinit var recyclerView: RecyclerView


    override fun onAttach(context: Context) {
        Log.e("ON ATTACH", "ON ATTACH")
        super.onAttach(context)

    }

    override fun onStart() {
        super.onStart()
        Log.e("ONSTART", "ON ATTACH")

        AccountSingleton.instance.ReloadRentCache(this.context, adapter)
        Log.e("Account size", AccountSingleton.instance.rentBooks.size.toString())
        Log.e("Adapter size", adapter.getItemCount().toString())
        adapter.notifyDataSetChanged()

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        slideshowViewModel =
            ViewModelProviders.of(this).get(SlideshowViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_slideshow, container, false)

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                activity?.finish()

            }
        }
        requireActivity().getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback)


        recyclerView = root.findViewById<RecyclerView>(R.id.recyclerView_rent)


        texts = ArrayList<String>()
        linearLayoutManager = LinearLayoutManager(this.context)
        recyclerView.layoutManager = linearLayoutManager
        adapter = RecyclerBookAdapter(AccountSingleton.instance.rentBooks)
        recyclerView.adapter = adapter



        return root
    }
}