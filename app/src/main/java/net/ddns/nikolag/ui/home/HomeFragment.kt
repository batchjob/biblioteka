package net.ddns.nikolag.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_home.*
import net.ddns.nikolag.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.sql.Date
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: RecyclerBookAdapter
    private lateinit var texts: ArrayList<String>
    private lateinit var bookList: ArrayList<BookClass>
    private lateinit var recyclerView: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

                // This callback will only be called when MyFragment is at least Started.
        /*
        OnBackPressedCallback callback = OnBackPressedCallback() {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
            }
        };*/
        super.onCreate(savedInstanceState)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                activity?.finish()

            }
        }
        requireActivity().getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback)


        recyclerView = root.findViewById<RecyclerView>(R.id.recyclerView)


        bookList = ArrayList<BookClass>()

        val name = "name"
        val ISBN = "name"
        val image = "name"
        val date = "name"
        val pages = "name"
        val author = "name"
        val lang = "name"
        val publisher = "name"

        val book: BookClass = BookClass(ISBN, name, image, date, pages, author, lang, publisher
        )
        bookList.add(book)



        texts = ArrayList<String>()
        linearLayoutManager = LinearLayoutManager(this.context)
        recyclerView.layoutManager = linearLayoutManager


        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val con = KolibriApiMaker()


        // register -> val params = con.callRegister("1", "2", "3", "4")
        val params = con.callGetAllBooks()

        val curr_context = this.context
        val queue = Volley.newRequestQueue(curr_context)


        val url = "https://nikolag.ddns.net/ian/index.php"

        val jsonOb = JSONObject()
        jsonOb.put("type", "register")

        Log.e("BEFOREW SEND", params.toString())

        val stringRequest = object: StringRequest(
            Request.Method.POST, url,
            object: Response.Listener<String>{
                override fun onResponse(response: String?) {
                   Log.d("Response GetBOoks",response.toString())
                   try{
                       val tryy = JSONArray(response)
                       JSONParser(tryy, null)

                       val bokkie = JSONBookParser(null, null).fromJsonArray(tryy)
                       Log.e("BOKKIIE", bokkie.toString())
                       adapter = RecyclerBookAdapter(bokkie)
                       recyclerView.adapter = adapter

                   }
                   catch (e: JSONException){

                   }

                }
            },
            object : Response.ErrorListener{
                override fun onErrorResponse(error: VolleyError?) {
                    Log.d("Error",error.toString())
                    Toast.makeText(curr_context, error?.message, Toast.LENGTH_SHORT).show()
                }
            }){
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

            override fun getParams(): MutableMap<String, String> {
                return params
            }
        }

        // Access the RequestQueue through your singleton class.
        queue.add(stringRequest)



        Log.d("tt", "Created")

    }
}