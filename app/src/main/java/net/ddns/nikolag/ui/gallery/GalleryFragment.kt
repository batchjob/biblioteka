package net.ddns.nikolag.ui.gallery

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_gallery.*
import kotlinx.android.synthetic.main.fragment_gallery.view.*
import net.ddns.nikolag.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class GalleryFragment : Fragment() {

    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: RecyclerBookAdapter
    private lateinit var texts: ArrayList<String>
    private lateinit var bookList: ArrayList<BookClass>
    private lateinit var recyclerView: RecyclerView


    private fun doSearch(){


        val con = KolibriApiMaker()

        Log.e("Text parse", this.editText.text.toString())


        // register -> val params = con.callRegister("1", "2", "3", "4")
        val params = con.callSearch(this.editText.text.toString() ,spinner.selectedItemPosition)

        val curr_context = this.context
        val queue = Volley.newRequestQueue(curr_context)


        val url = "https://nikolag.ddns.net/ian/index.php"

        val jsonOb = JSONObject()
        jsonOb.put("type", "register")

        Log.e("BEFOREW SEND", params.toString())

        val fabView = this.fab.findViewById<FloatingActionButton>(R.id.fab)

        val stringRequest = object: StringRequest(
            Request.Method.POST, url,
            object: Response.Listener<String>{
                override fun onResponse(response: String?) {
                    Log.d("Search response",response.toString())


                    try{

                        val tryy = JSONArray(response)
                        JSONBookParser(null, null)
                        //bookList
                        bookList.clear()
                        bookList.addAll(JSONBookParser(null, null).fromJsonArray(tryy))

                        Log.e("Recv sizte", bookList.size.toString())
                        adapter.notifyDataSetChanged()

                    }

                    catch (e: JSONException){
                        Snackbar.make(fabView, R.string.response_noresults, Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show()

                    }


                }
            },
            object : Response.ErrorListener{
                override fun onErrorResponse(error: VolleyError?) {
                    Log.d("Error",error.toString())
                    Toast.makeText(curr_context, error?.message, Toast.LENGTH_SHORT).show()
                }
            }){
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

            override fun getParams(): MutableMap<String, String> {
                return params
            }
        }

        // Access the RequestQueue through your singleton class.
        queue.add(stringRequest)


    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
            ViewModelProviders.of(this).get(GalleryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)

        super.onCreate(savedInstanceState)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                activity?.finish()

            }
        }
        requireActivity().getOnBackPressedDispatcher().addCallback(this, onBackPressedCallback)

        root.editText.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){

                this.doSearch()

                true
            } else {
                false
            }
        }

        val spinner: Spinner = root.spinner
        ArrayAdapter.createFromResource(
            root.context,
            R.array.searchby,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        recyclerView = root.findViewById<RecyclerView>(R.id.recyclerView21)


        bookList = ArrayList<BookClass>()



        linearLayoutManager = LinearLayoutManager(this.context)
        recyclerView.layoutManager = linearLayoutManager

        adapter = RecyclerBookAdapter(bookList)
        recyclerView.adapter = adapter



        val fab: FloatingActionButton = root.fab
        fab.setOnClickListener { view ->

            this.doSearch()
        }



        return root
    }
}