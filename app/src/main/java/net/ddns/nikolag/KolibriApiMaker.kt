package net.ddns.nikolag

import android.util.Log
import org.json.JSONObject


open class KolibriApiMaker {

    open fun printVarName() {
        print("I am in class printVarName")
    }

    init {

    }



    open fun getForRegister(name: String, priimek: String, password: String, email: String ): JSONObject {
        val params = JSONObject()
        params.put("type", "register")
        params.put("name", name)
        params.put("priimek", priimek)
        params.put("password", password)
        params.put("email", email)
        return params
    }

    open fun getForRegister2(name: String, priimek: String, password: String, email: String ): String{
        var pls = "type=register, "
        pls = pls + "name=" + name
        pls = pls + ", priimek=" + priimek
        pls = pls + ", password=" + password
        pls = pls + ", email=" + email
        return pls
    }

    open fun callRegister(name: String, priimek: String,email: String, password: String) : HashMap<String, String>{
        val params = HashMap<String, String>()
        params["type"] = "register"
        params["name"] = name
        params["priimek"] = priimek
        params["password"] = password
        params["email"] = email

        Log.e("REG DEBUG", params.toString())

        return params
    }

    open fun callLogin(email: String, password: String) : HashMap<String, String>{
        val params = HashMap<String, String>()
        params["type"] = "login"
        params["password"] = password
        params["email"] = email
        return params
    }

    open fun callGetAllBooks() : HashMap<String, String>{
        val params = HashMap<String, String>()
        params["type"] = "allBooks"
        return params
    }

    open fun callRent(email: String, book_name: String) : HashMap<String, String>{
        val params = HashMap<String, String>()
        params["type"] = "rent"
        params["email"] = email
        params["book"] = book_name
        return params
    }

    open fun callReturnBook(email: String, book_name: String) : HashMap<String, String>{
        val params = HashMap<String, String>()
        params["type"] = "returnBook"
        params["email"] = email
        params["book"] = book_name
        return params
    }

    open fun callMyBooks(email: String) : HashMap<String, String>{
        val params = HashMap<String, String>()
        params["type"] = "myBooks"
        params["email"] = email
        return params
    }

    open fun searchOptions(): ArrayList<String>{
        val arlist = ArrayList<String>()

        arlist.add("byAuthorName")
        arlist.add("yearOfRelease")
        arlist.add("byGenre")
        arlist.add("byISBN")

        return arlist
    }

    open fun searchOptionsDict(): HashMap<String, String> {
        val arlist = HashMap<String, String>()

        arlist["Ime autorja"] = "byAuthorName"
        arlist["leto"] = "yearOfRelease"
        arlist["isbn"] = "byISBN"
        arlist["žanr"] = "byGenre"

        return arlist
    }

    open fun getStringForSearch(param: String): String?{
        return this.searchOptionsDict()[param]
    }

    open fun searchIndexes(): ArrayList<String>{
        val arlist = ArrayList<String>()

        arlist.add("byAuthorName")
        arlist.add("yearOfRelease")
        arlist.add("byISBN")
        arlist.add("byGenre")

        return arlist
    }

    open fun paramFromIndex(param: Int): String{
        return this.searchIndexes().get(param)
    }

    open fun callSearch(query: String, index: Int): HashMap<String, String>{
        val params = HashMap<String, String>()
        params["type"] = this.paramFromIndex(index)
        params["variable"] = query
        return params
    }

}