package net.ddns.nikolag

import android.app.Application
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.core.graphics.component1
import com.android.volley.*
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject



interface ServiceInterface {
    fun post(path: String, params: JSONArray, completionHandler: (response: JSONArray?) -> Unit)
    fun get(path: String, completionHandler: (response: JSONArray?) -> Unit)
}


class VolleyRequester : Application(){

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(applicationContext)
            }
            return field
        }

    fun <T> addToRequestQueue(request: Request<T>, tag: String) {
        request.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue?.add(request)
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        request.tag = TAG
        requestQueue?.add(request)
    }

    fun cancelPendingRequests(tag: Any) {
        if (requestQueue != null) {
            requestQueue!!.cancelAll(tag)
        }
    }

    companion object {
        private val TAG = VolleyRequester::class.java.simpleName
        @get:Synchronized var instance: VolleyRequester? = null
            private set
    }
}

class APIController constructor(serviceInjection: ServiceInterface): ServiceInterface {
    private val service: ServiceInterface = serviceInjection

    override fun post(path: String, params: JSONArray, completionHandler: (response: JSONArray?) -> Unit) {
        service.post(path, params, completionHandler)
    }

    override fun get(path: String, completionHandler: (response: JSONArray?) -> Unit) {
        service.get(path, completionHandler)
    }
}

class ServiceVolley : ServiceInterface {
    val TAG = ServiceVolley::class.java.simpleName
    val basePath = "https://nikolag.ddns.net/ian/index.php"

    override fun post(path: String, params: JSONArray, completionHandler: (response: JSONArray?) -> Unit) {
/*
        //StringRequest()
        val opala = object: StringRequest(Method.GET, basePath,

            Response.Listener<String> { response ->
            Log.d(TAG, "/post request OK! Response: $response")
            //completionHandler(response)
        },
            Response.ErrorListener { error ->

                Log.e("DRECNI" ,"DREK")
                Log.e("EROR", error.toString())
                VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                completionHandler(null)
            }){
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }}*/

        val jsonObjReq = object : JsonArrayRequest(Method.POST, basePath, params,
            Response.Listener<JSONArray> { response ->
                Log.d(TAG, "/post request OK! Response: $response")
                completionHandler(response)
            },
            Response.ErrorListener { error ->
                Log.e("E", error.message);
                VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                completionHandler(null)
            })

        {
/*
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }*/


            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                val params2 = HashMap<String, String>()
                val ass2 = KolibriApiMaker().getForRegister2("1","w","3", "1s")
                val ass3 = JSONArray()
               // ass3.put("tyte", "tity")

                val kaj = params2.toString().substring(1, params2.toString().length - 1)
                Log.e("PARAM ASS", ass2)
                return ass2.toByteArray()
            }
        }

        VolleyRequester.instance?.addToRequestQueue(jsonObjReq, TAG)
    }

    override fun get(path: String, completionHandler: (response: JSONArray?) -> Unit) {
        /*
        val jsonObjReq = object : JsonObjectRequest(Method.POST, basePath + path,
            Response.Listener<JSONObject> { response ->
                Log.d(TAG, "/post request OK! Response: $response")
                completionHandler(response)
            },
            Response.ErrorListener { error ->
                VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                completionHandler(null)
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }

        VolleyRequester.instance?.addToRequestQueue(jsonObjReq, TAG)

         */
        val jsonObjReq = object : JsonArrayRequest(Method.GET, basePath, null,
            Response.Listener<JSONArray> { response ->
                Log.d(TAG, "/post request OK! Response: $response")
                completionHandler(response)
            },
            Response.ErrorListener { error ->
                Log.e("E", error.message);
                VolleyLog.e(TAG, "/post request fail! Error: ${error.message}")
                completionHandler(null)
            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Content-Type", "application/json")
                return headers
            }
        }

        VolleyRequester.instance?.addToRequestQueue(jsonObjReq, TAG)


    }
}
