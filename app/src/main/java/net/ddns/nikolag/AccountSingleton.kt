package net.ddns.nikolag

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

class AccountSingleton private constructor() {

    lateinit var name: String
    lateinit var email: String
    lateinit var rentBooks: ArrayList<BookClass>

    private object HOLDER {
        val INSTANCE = AccountSingleton()
    }

    companion object {
        val instance: AccountSingleton by lazy { HOLDER.INSTANCE }
    }

    fun isBookRented(bookName: String): Boolean{
        for (book in rentBooks){
            if(bookName == book.name)
                return true
        }
        return false
    }

    fun ReloadRentCache(context: Context?, adapter: RecyclerBookAdapter?, callbck: (() -> Unit)? = null){

        val con = KolibriApiMaker()
        val params = con.callMyBooks(instance.email)

        val queue = Volley.newRequestQueue(context)


        val url = "https://nikolag.ddns.net/ian/index.php"

        val jsonOb = JSONObject()
        jsonOb.put("type", "register")

        Log.e(AccountSingleton::class.java.name, "Flushing rent cache")
        instance.rentBooks .clear()

        val stringRequest = object: StringRequest(
            Request.Method.POST, url,
            object: Response.Listener<String>{
                override fun onResponse(response: String?) {
                    Log.d("Search response",response.toString())


                    try{

                        val tryy = JSONArray(response)
                        JSONBookParser(null, null)
                        //bookList
                        instance.rentBooks .addAll(JSONBookParser(null, null).fromJsonArray(tryy))

                        Log.e("Recv sizte", instance.rentBooks .size.toString())
                        adapter?.notifyDataSetChanged()

                    }

                    catch (e: JSONException){

                    }
                    callbck?.invoke()
                }

            },
            object : Response.ErrorListener{
                override fun onErrorResponse(error: VolleyError?) {
                    Log.d("Error",error.toString())
                }
            }){
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                return headers
            }

            override fun getParams(): MutableMap<String, String> {
                return params
            }
        }
        queue.add(stringRequest)

    }

}