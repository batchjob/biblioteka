package net.ddns.nikolag

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputEditText
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        findViewById<Button>(R.id.toLogin).setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)

            startActivity(intent)
            finish()

        }

        findViewById<Button>(R.id.do_register).setOnClickListener{

            val reg_name = findViewById<TextInputEditText>(R.id.registerName)
            val reg_surname = findViewById<TextInputEditText>(R.id.registerPriimek)
            val reg_email = findViewById<TextInputEditText>(R.id.registerEmail)
            val reg_password = findViewById<TextInputEditText>(R.id.registerPassword)

            val con = KolibriApiMaker()

            val params = con.callRegister(reg_name.text.toString(), reg_surname.text.toString(),reg_email.text.toString(), reg_password.text.toString())

            val queue = Volley.newRequestQueue(this)
            val url = "https://nikolag.ddns.net/ian/index.php"

            Log.e("BEFOREW SEND", params.toString())

            val stringRequest = object: StringRequest(
                Request.Method.POST, url,
                object: Response.Listener<String>{
                    override fun onResponse(response: String?) {
                        Log.d("Response",response)

                        if(response?.trim() == "1"){
                            // start activity
                            Toast.makeText(this@RegisterActivity, R.string.register_success, Toast.LENGTH_LONG).show()
                            val intent = Intent(this@RegisterActivity, LoginActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else{
                            var toast_str = resources.getString(R.string.register_exists)
                            if(response?.trim()?.length!! > 0){
                                toast_str = response.trim()
                            }
                            Toast.makeText(this@RegisterActivity, toast_str, Toast.LENGTH_LONG).show()
                        }
                    }
                },
                object : Response.ErrorListener{
                    override fun onErrorResponse(error: VolleyError?) {
                        Log.d("Error",error.toString())
                        Log.e("KAJ", "KAJ")
                        var trex = 121
                        trex = trex + 2
                    }
                }){
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/x-www-form-urlencoded"
                    return headers
                }

                override fun getParams(): MutableMap<String, String> {
                    return params
                }
            }

            queue.add(stringRequest)

            return@setOnClickListener
        }
    }
}
