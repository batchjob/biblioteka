package net.ddns.nikolag

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_book_info.*
import kotlinx.android.synthetic.main.activity_book_info.view.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.lang.ref.WeakReference
import java.util.HashMap

class BookViewActivity : AppCompatActivity() {

    lateinit var rentButton: Button
    lateinit var bookName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_info)


        this.text_name_value.setText(this.intent?.extras?.get("BOOK_NAME").toString())
        this.text_isbn_value.setText(this.intent?.extras?.get("BOOK_ISBN").toString())
        this.text_pages_value.setText(this.intent?.extras?.get("BOOK_PAGES").toString())
        this.text_date_value.setText(this.intent?.extras?.get("BOOK_DATE").toString())
        this.text_author_value.setText(this.intent?.extras?.get("BOOK_AUTHOR").toString())
        this.text_publisher_value.setText(this.intent?.extras?.get("BOOK_PUBLISHER").toString())


        bookName = this.intent?.extras?.get("BOOK_NAME").toString()

        val linkstr = this.intent?.extras?.get("BOOK_IMG")

        rentButton = Button(this)
        rentButton.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.MATCH_PARENT)
        (rentButton.layoutParams as LinearLayout.LayoutParams).weight = 0.2f;
        (rentButton.layoutParams as LinearLayout.LayoutParams).leftMargin = 6
        (rentButton.layoutParams as LinearLayout.LayoutParams).rightMargin = 6
        rentButton.setText("vrni")
        rentButton.setTextColor(this.button2.textColors)
        var kopija = this.button2.getBackground()
        val dr_copy = this.button2.background.constantState?.newDrawable()?.mutate()
        rentButton.background = dr_copy
        rentButton.setOnClickListener {
            val con = KolibriApiMaker()

            val params = con.callReturnBook(AccountSingleton.instance.email, bookName)



            val curr_context = this
            val queue = Volley.newRequestQueue(curr_context)


            val url = "https://nikolag.ddns.net/ian/index.php"


            Log.e("BEFOREW SEND", params.toString())

            val me = this

            val stringRequest = object: StringRequest(
                Request.Method.POST, url,
                object: Response.Listener<String>{
                    override fun onResponse(response: String?) {
                        Log.d("Response rent",response)

                        if(response?.trim() == "1"){
                            // rloead cache
                            AccountSingleton.instance.ReloadRentCache(curr_context, null, {

                                if(AccountSingleton.instance.isBookRented(bookName) == true){

                                    me.addBtn()
                                }else{

                                    me.removeBtn()
                                }
                                me.linearLayout.requestLayout()

                            })

                        }

                    }
                },
                object : Response.ErrorListener{
                    override fun onErrorResponse(error: VolleyError?) {
                        Log.d("Error",error.toString())
                        Toast.makeText(curr_context, error?.message, Toast.LENGTH_SHORT).show()
                    }
                }){
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/x-www-form-urlencoded"
                    return headers
                }

                override fun getParams(): MutableMap<String, String> {
                    return params
                }
            }

            // Access the RequestQueue through your singleton class.
            queue.add(stringRequest)


        }
        Log.e("NEXT LOG", "KILL!")
        if(AccountSingleton.instance.isBookRented(bookName) == true){
            Log.e("ADDING BTN","")
            this.addBtn()
        }else{
            Log.e("REMOVING BTN","")
            this.removeBtn()
        }



        Glide.with(this).load(linkstr).into(this.imageView2)


        KolibriApiMaker().searchOptionsDict()


        val book_name = this.intent?.extras?.get("BOOK_NAME").toString()
        val user_email = AccountSingleton.instance.email

        this.button2.setOnClickListener {


            val con = KolibriApiMaker()

            val params = con.callRent(user_email, book_name)



            val curr_context = this
            val queue = Volley.newRequestQueue(curr_context)


            val url = "https://nikolag.ddns.net/ian/index.php"


            Log.e("BEFOREW SEND", params.toString())

            val me = this

            val stringRequest = object: StringRequest(
                Request.Method.POST, url,
                object: Response.Listener<String>{
                    override fun onResponse(response: String?) {
                        Log.d("Response rent",response)

                        if(response?.trim() == "1"){
                            // rloead cache
                            AccountSingleton.instance.ReloadRentCache(curr_context, null, {

                                if(AccountSingleton.instance.isBookRented(bookName) == true){

                                    me.addBtn()
                                }else{

                                    me.removeBtn()
                                }
                                me.linearLayout.requestLayout()

                            })

                        }

                    }
                },
                object : Response.ErrorListener{
                    override fun onErrorResponse(error: VolleyError?) {
                        Log.d("Error",error.toString())
                        Toast.makeText(curr_context, error?.message, Toast.LENGTH_SHORT).show()
                    }
                }){
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/x-www-form-urlencoded"
                    return headers
                }

                override fun getParams(): MutableMap<String, String> {
                    return params
                }
            }

            queue.add(stringRequest)

        }

    }

    private fun removeBtn(){
        this.linearLayout.removeView(rentButton)
    }

    private fun addBtn(){
        this.removeBtn()
        this.linearLayout.addView(rentButton)
    }

}
